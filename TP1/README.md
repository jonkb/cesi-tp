# **TP2:Serveur Web**
# **I.Base de données**
Sur db

`yum install mariadb mariadb-server`
`sudo ss -alnpt`

Le port utilisé par Mariadb est le 3306

```
systemctl enable mariadb
systemctl start mariadb
```

`sudo mysql -u root -p`


```
CREATE DATABASE wordpress;
Query OK, 1 row affected (0.00 sec)
CREATE USER cesi@localhost IDENTIFIED BY 'cesi';
Query OK, 1 row affected (0.00 sec)
GRANT ALL PRIVILEGES ON 'wordpress'.* TO 'cesi@%' IDENTIFIED BY 'cesi';
Query OK, 1 row affected (0.00 sec)
FLUSH PRIVILEGES;
Query OK, 1 row affected (0.00 sec)
Vérif
SHOW GRANTS FOR 'root'@'%';
+-----------------------------------------------------------------------------------------------------+
| Grants for cesi@%                                                                                   |
+-----------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'cesi'@'%' IDENTIFIED BY PASSWORD '*CA01C7038F2D790969629D84CDF7897B6C7F0622' |
| GRANT ALL PRIVILEGES ON `wordpress`.* TO 'cesi'@'%'                                                 |
+-----------------------------------------------------------------------------------------------------+

exit
```

# **II.Server Web**

Sur les deux machines web et rp

```
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --permanent --zone=public --add-service=http
```

Sur les deux machines db et web
`sudo firewall-cmd --add-port=3306/tcp --permanent                #Port mariadb`

`sudo firewall-cmd --reload`

Sur web

`yum install mariadb`

`yum install httpd`
`sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y`
`sudo yum install -y yum-utils`

On supprime d'éventuelles vieilles versions de PHP précédemment installées
`sudo yum remove -y php`

Activation du nouveau dépôt

`sudo yum-config-manager --enable remi-php56`


Installation de PHP 5.6.40 et de librairies récurrentes
`sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y`

```
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --add-port=3306/tcp --permanent                #Port mariadb
```

`sudo firewall-cmd --reload`

`sudo yum install wget`

```
cd /tp2
sudo wget http://wordpress.org/latest.tar.gz
```
`sudo tar -xzvf latest.tar.gz`
`sudo mv wordpress/ /var/www/html/`

Configurer la db sur la page web ou créer le fichier de conf à partir de wp-config-sample.php et le nommer wp-config.php
`cd /var/www/html/`
`cp wp-config-sample.php wp-config.php`

Modifier les infos db

# **III.Reverse Proxy**

Sur rp

`sudo yum install epel-release`
`sudo yum update`

`sudo yum install nginx`

```
sudo systemctl start nginx
sudo systemctl enable nginx
```

```
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
```

`sudo vi /etc/nginx/nginx.conf`
```
remplacer 
server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://10.99.99.11;
        }
    }
```

`sudo systemctl reload nginx`

Sur web 

Enlever règle http

# **IV.Un peu de sécu**
## **1.fail2ban**

Fail 2 ban 
Sur les 3 VM

`sudo yum install epel-release`
`sudo yum install fail2ban fail2ban-systemd`

`sudo cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`

`sudo vi /etc/fail2ban/jail.local`

Modifier ligne ignoreip avec l'adresse du serveur

`sudo touch /etc/fail2ban/jail.d/sshd.local`
`sudo chmod +x /etc/fail2ban/jail.d/sshd.local`

`sudo vi /etc/fail2ban/jail.d/sshd.local`
Ajouter

```
[sshd]
enabled = true
port = ssh
#action = firewallcmd-ipset
logpath = %(sshd_log)s
maxretry = 5
bantime = 86400
```

`sudo systemctl enable fail2ban`
`sudo systemctl start fail2ban`

## **2.HTTPS**



`sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.tp2.cesi.key -out web.tp2.cesi.crt`

Mettre le FQDN du serveur web web.tp2.cesi

Changer le dossier des clés pour les mettres dans celui prévu de centos

mv web.cesi.crt /etc/pki/certs/
mv web.cesi.key /etc/pki/private/

Copier texte dans le fichier de conf de nginx

`sudo vi /etc/nginx/nginx.conf`
```
server {
          listen       443 ssl;
          
          ssl on;
        
          server_name  web.tp2.cesi;

          ssl_certificate "/etc/pki/tls/certs/web.cesi.crt";
          ssl_certificate_key "/etc/pki/tls/private/web.cesi.key";

          location / {
                proxy_pass   http://10.99.99.11;
        }
    }
```
`sudo systemctl restart nginx`

La commande `ss -alnpt` permettra de vérifier si l'application écoute bien sur le port pour debug
