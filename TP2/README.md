# **TP1:Configuration système**
# **I. Utilisateur**

## **1. Création et configuration**

`sudo groupadd admins`

`sudo visudo`
Modifier dans le fichier
```
admins ALL=(ALL)    ALL

admins ALL=(ALL)    NOPASSWD: ALL
```

`sudo usermod -aG admins cesi`

## **2. SSH**

Sur OS Client (Win)

`ssh-keygen -t rsa -b 4096`

Sur CentOS 7

```
cd
mkdir .ssh
cd .ssh
vim authorized_keys
```
copier la clé
```
chmod 600 /home/user/.ssh/authorized_keys
chmod 700 /home/user/.ssh
```

# **II. Configuration réseau**
## **1.Nom de domaine**
```
sudo hostname vm1.centos
echo 'vm1.centos' | sudo tee /etc/hostname
```

Vérif
`hostname`

## **2.Serveur DNS**
`sudo cat /etc/resolv.conf `              Déja configurer lors de l'install. Retourne 8.8.8.8

# **III. Partitionemment**
## **1.Préparation de la VM**

Ajout de deux disque sur virtualbox de 3go chacun.

## **2.Partitionemment**
```
sudo pvcreate /dev/sdb
sudo pvcreate /dev/sdc
```

Vérif
`sudo pvs`

`sudo vgcreate data /dev/sdb /dev/sdc`

Vérif
`sudo vgs`

```
sudo lvcreate -L 2G data -n part1
sudo lvcreate -L 2G data -n part2
sudo lvcreate -L 1.99G data -n part3
```

```
mkfs -t ext4 /dev/data/part1
mkfs -t ext4 /dev/data/part2
mkfs -t ext4 /dev/data/part3
```

```
mkdir /mnt/part1
mkdir /mnt/part2
mkdir /mnt/part3
```

```
sudo mount /dev/data/part1 /mnt/part1
sudo mount /dev/data/part2 /mnt/part2
sudo mount /dev/data/part3 /mnt/part3
```

`vim /etc/fstab`

```
/dev/data/part1 /mnt/part1 ext4 defaults 0 0
/dev/data/part3 /mnt/part2 ext4 defaults 0 0
/dev/data/part3 /mnt/part3 ext4 defaults 0 0
```

Vérif
`sudo umount /mnt/part1`            Démonter la partition si elle est déjà montée
`sudo mount -av `                   Démonter la partition, en utilisant les infos renseignées dans `/etc/fstab`
`sudo reboot `                      Non nécessaire si le mount -av fonctionne correctement

# **IV.Gestion de services**
## **1.Interaction avec un service existant**

`systemctl status firewalld.service`  Chargé et active

## **2.Création de service**

`sudo vi /etc/systemd/system/web.service`

### **A.Unité simpliste**
```
sudo firewall-cmd --add-port=8888/tcp --permanent
sudo firewall-cmd --reload
```

```
sudo systemctl daemon-reload
sudo systemctl status web
sudo systemctl start web
sudo systemctl enable web
```

### **B.Modification de l'unité**

`sudo vi /etc/systemd/system/web.service`
```
[SERVICE]
User=web
WorkingDirectory=/srv
```

```
systemctl daemon-reload
systemctl restart web.service
sudo systemctl status web.service
